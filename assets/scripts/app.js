var app = angular.module("myApp", ['ui.router' , 'ngAnimate']);
app.controller("headerCtrl" , function($scope , $rootScope, $location  ) {
    $scope.siteTitle = "Github Users";
    $scope.location = $location.path();
    $rootScope.$on('$routeChangeSuccess', function() {
        $scope.location = $location.path();
    });


});

app.run([ '$rootScope', '$state', '$stateParams',
function ($rootScope, $state, $stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

    // handle Redirect to 
    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
      if (to.redirectTo) {
        evt.preventDefault();
        $state.go(to.redirectTo, {id: 1})
      }
    });

}]);

app.config(function($stateProvider , $urlRouterProvider , $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl : "partials/partial-home.html",
            controller: "homeController",
            controllerAs:"homeCtrl",
            data: {
              pageTitle: 'Homepage'
            }
        })
        .state('users', {
            url: '/users',
            templateUrl : "partials/partial-users.html",
            controller: "userController",
            controllerAs:"userCtrl",
            data: {
              pageTitle: 'Users List'
            },
            resolve: {
                usersList : function($http) {
                return $http.get("https://api.github.com/users")
                    .then(function(response) {
                            return response.data;
                    }); 
                },
                //   onEnter: function($state){
                //   //   $state.go("users.details", {id: 1});
                // }
            },
            redirectTo: 'users.details',
        })
        .state('users.details', {
            url: "/:id",
            views : {
                'details@users' : {
                    templateUrl : "partials/partial-user-details.html",
                    controller: "userDetailsController",
                    controllerAs: "userDetailsCtrl",
                }
            },
            data: { pageTitle: 'User Details' },
            resolve: {
                userDetails : function($http , $stateParams) {
                return $http.get("https://api.github.com/users/"+ $stateParams.id)
                    .then(function(response) {
                            return response.data;
                    }); 
                }
            },
        })
        .state('about', {
            templateUrl : "partials/partial-about.html",
            controller: "aboutController",
            controllerAs: "aboutCtrl",
            data: {
              pageTitle: 'About Us'
            },
        });

    });
  

