app.controller("userController" , function($scope ,  $location , usersList ,$state  ) {
    $scope.users = usersList;
    $scope.limit = 5;
    $scope.$state = $state;
    // loading more 
    // $scope.tempro = templateProvider;
    // console.log($scope.tempro);
    $scope.loadMore = function() {
      var increamented = $scope.limit + 3;
      $scope.limit = increamented > $scope.users.length ? $scope.users.length : increamented;
    };


   //  $state.go("users.details", {id: 1});
});


app.controller("userDetailsController" , function($scope , $stateParams , $state , userDetails) {
        $scope.userDetails = userDetails;
        $scope.$state = $state;   
});
